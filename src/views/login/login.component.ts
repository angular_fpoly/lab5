import { Component } from '@angular/core';
import {AdminLayoutComponent} from "../../layouts/admin-layout/admin-layout.component";
import {FormsModule} from "@angular/forms";
import { NgForm } from '@angular/forms';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    AdminLayoutComponent,
    FormsModule,
    RouterLink
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  userData = {
    username: '',
    password: ''
  }
  onSubmit(contactForm : NgForm) {
    console.log(contactForm.value);
    console.log(this.userData)
  }
  suggest() {
    this.userData.username = 'TienFpoly@fe.edu.vn';
  }
}
