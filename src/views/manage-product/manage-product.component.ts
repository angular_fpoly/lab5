import { Component } from '@angular/core';
import {AdminLayoutComponent} from "../../layouts/admin-layout/admin-layout.component";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-manage-product',
  standalone: true,
  imports: [
    AdminLayoutComponent,
    ReactiveFormsModule,
    RouterLink
  ],
  templateUrl: './manage-product.component.html',
  styleUrl: './manage-product.component.css'
})
export class ManageProductComponent {
  productForm! : FormGroup
  constructor(){}
  ngOnInit() {
    this.productForm = new FormGroup({
      'productName': new FormControl('123', [Validators.required]),
      "productCode": new FormControl('123-321', [Validators.required]),
      "description": new FormControl('test-product description', [Validators.required]),
      "price": new FormControl(7000, [Validators.required]),
      "starRating": new FormControl(5, [Validators.required]),
      "imageUrl": new FormControl('https://img.freepik.com/free-photo/vip-typography-3d-golden-font_53876-104193.jpg', [Validators.required]),
    })
  }

  onSubmit() {
    console.log(this.productForm.get('productName')?.value);
    console.log(this.productForm.value)
  }
}
