import { Routes } from '@angular/router';
import {DashboradComponent} from "../views/dashborad/dashborad.component";
import {HomeComponent} from "../views/home/home.component";
import {LoginComponent} from "../views/login/login.component";
import {ManageProductComponent} from "../views/manage-product/manage-product.component";

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'admin', component: DashboradComponent},
  { path: 'admin/login', component: LoginComponent},
  { path: 'admin/product', component: ManageProductComponent}
];
